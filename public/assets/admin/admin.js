tinymce.init({
    selector: '.tinymce',
    language: 'uk',
    toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image media | link",
    plugins: "image imagetools media link",
    body_class: 'content-text',
    file_browser_callback: elFinderBrowser,
    paste_data_images: true
});
