<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\ArticleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category/{slug}", name="category-list")
     */
    public function list(Category $category, Request $request, ArticleRepository $articleRepository, PaginatorInterface $paginator)
    {
        $articles = $paginator->paginate(
            $articleRepository->getQBCategory($category),
            $request->query->getInt('page', 1),7
        );

        return $this->render('category/list.html.twig', [
            'articles' => $articles,
            'category' => $category
        ]);
    }
}
