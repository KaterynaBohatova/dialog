<?php


namespace App\Controller;


use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RSSController extends AbstractController
{

    /**
     * @Route("/rss/latest", name="rss_latest")
     * @param ArticleRepository $articleRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function latest(ArticleRepository $articleRepository)
    {
        $articles = $articleRepository->findBy([], [
            'publishedAt' => 'DESC',
        ], 10);

        $response = $this->render('rss/latest.rss.twig', [
            'articles' => $articles,
        ]);

        $response->headers->set('Content-Type', 'application/rss+xml; charset=UTF-8');

        return $response;
    }

}