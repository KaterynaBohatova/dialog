<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article/view/{slug}/", name="article-view")
     */
    public function view(Request $request, Article $article, ArticleRepository $articleRepository, EntityManagerInterface $em)
    {
        $related = $articleRepository
            ->getQBCategory($article->getCategories()->first())
            ->setMaxResults(4)
            ->orderBy('a.publishedAt', 'DESC')
            ->getQuery()
            ->execute()
        ;

        $article->setViews($article->getViews() + rand(1, 10));
        $em->flush();

        $comment = new Comment();
        $comment->setArticle($article);

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var EntityManagerInterface $em */
            $em = $this->getDoctrine()->getManager();

            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute(
                'article-view',
                [
                    'slug' => $article->getSlug()
                ]
            );
        }


        return $this->render('article/view.html.twig', [
            'article' => $article,
            'related' => $related,
            'form' => $form->createView(),
        ]);
    }
}
