<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(ArticleRepository $articleRepository)
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'top' => $articleRepository->findByTagId(Tag::TOP, 10),
            'main' => $articleRepository->findByTagId(Tag::MAIN, 7),
            'trending' => $articleRepository->findByTagId(Tag::TRENDING, 3),
            'article' => $articleRepository->findBy([], ['publishedAt' => 'DESC'], 10),
        ]);
    }

    public function sidebar(ArticleRepository $articleRepository, TagRepository $tagRepository)
    {
        return $this->render('default/sidebar.html.twig', [
            'popular' => $articleRepository->findByTagId(Tag::POPULAR, 5),
            'trending' => $articleRepository->findByTagId(Tag::TRENDING, 3),
            'trending_main' => $articleRepository->findByTagId(Tag::TRENDING_MAIN, 1),
            'tags' => $tagRepository->findPopular()
        ]);
    }

    public function footerBox(ArticleRepository $articleRepository, CategoryRepository $categoryRepository)
    {
        return $this->render('default/footer-box.html.twig', [
            'latest' => $articleRepository->findBy([], ['publishedAt' => 'DESC'], 3),
            'categories' => $categoryRepository->findAll(),
            'commented' => $articleRepository->findMostCommented(3),
        ]);
    }

    public function menu(CategoryRepository $categoryRepository)
    {
        return $this->render('default/menu.html.twig', [
            'categories' => $categoryRepository->findAll(),
        ]);
    }

}
