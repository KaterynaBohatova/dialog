<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Repository\ArticleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class TagController extends AbstractController
{
    /**
     * @Route("/tag/{id}", name="tag-list")
     */
    public function list(Tag $tag, Request $request, ArticleRepository $articleRepository, PaginatorInterface $paginator)
    {
        $articles = $paginator->paginate(
            $articleRepository->getQBTag($tag),
            $request->query->getInt('page', 1),7
        );

        return $this->render('tag/list.html.twig', [
            'articles' => $articles,
            'tag' => $tag
        ]);
    }
}
