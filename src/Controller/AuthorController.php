<?php

namespace App\Controller;

use App\Entity\Author;
use App\Repository\ArticleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController extends AbstractController
{
    /**
     * @Route("/author/{id}/", name="author-list")
     */
    public function list(Author $author, Request $request, ArticleRepository $articleRepository, PaginatorInterface $paginator)
    {
        $articles = $paginator->paginate(
            $articleRepository
                ->getQBByAuthor($author)
                ->orderBy('a.publishedAt', 'DESC'),
            $request->query->getInt('page', 1),7
        );

        return $this->render('author/list.html.twig', [
            'articles' => $articles,
            'author' => $author
        ]);

    }
}
