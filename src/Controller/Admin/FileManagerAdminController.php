<?php

namespace App\Controller\Admin;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FileManagerAdminController extends Controller
{
    /**
     * @Route("/admin/files", name="file_manager")
     */
    public function indexAction(Request $request)
    {
        return $this->render('admin/files/index.html.twig');
    }

}
