<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Tag;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190404065223 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {

        $this->addSql('INSERT INTO tag (id, name) VALUES ('. Tag::TRENDING_MAIN .', "Головна в тренді")' );

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DELETE FROM tag WHERE id IN ('. Tag::TRENDING_MAIN .')' );

    }
}
