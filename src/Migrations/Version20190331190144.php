<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Tag;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190331190144 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('INSERT INTO tag (id, name) VALUES ('. Tag::TOP .', "Toп")' );
        $this->addSql('INSERT INTO tag (id, name) VALUES ('. Tag::MAIN .', "Головна")' );
        $this->addSql('INSERT INTO tag (id, name) VALUES ('. Tag::POPULAR .', "Популярні")' );
        $this->addSql('INSERT INTO tag (id, name) VALUES ('. Tag::TRENDING .', "В тренді")' );

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DELETE FROM tag WHERE id IN ('. Tag::TOP .', '. Tag::MAIN .', '. Tag::POPULAR .', '. Tag::TRENDING .')' );

    }
}
