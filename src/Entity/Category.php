<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @UniqueEntity("slug", message="Значення вже використовується")
 * @Vich\Uploadable
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Article", mappedBy="categories")
     */
    private $articles;

    /**
     * @ORM\Column(type="string", length=250, unique=true)
     * @Assert\Regex(pattern="/^\w+/", message="Виключно англійською")
     */
    private $slug;

    /**
     * @Vich\UploadableField(mapping="category_image", fileNameProperty="fileName")
     * @var File
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $fileName;

    /**
     * @var \DateTime
     * @ORM\Column(name="update_at", type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $ukrNetCategory;

    public function __construct()
    {
        $this->articles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return File
     */
    public function getImage(): ? File
    {
        return $this->image;
    }

    /**
     * @param File $image
     * @return Category
     */
    public function setImage(?File $image): Category
    {
        if (null !== $image) {
            $this->image = $image;
            $this->updateAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     * @return Category
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateAt(): \DateTime
    {
        return $this->updateAt;
    }

    /**
     * @param \DateTime $updateAt
     * @return Category
     */
    public function setUpdateAt(\DateTime $updateAt): Category
    {
        $this->updateAt = $updateAt;
        return $this;
    }

    public function getUkrNetCategory(): ?string
    {
        return $this->ukrNetCategory;
    }

    public function setUkrNetCategory(?string $ukrNetCategory): self
    {
        $this->ukrNetCategory = $ukrNetCategory;

        return $this;
    }



}
