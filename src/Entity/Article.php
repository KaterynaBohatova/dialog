<?php

namespace App\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @Vich\Uploadable
 */
class Article
{
    /**
     * @ORM\Id()

     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $shortContent;

    /**
     * @ORM\Column(type="datetime")
     */
    private $publishedAt;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $source;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", inversedBy="articles")
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="articles", cascade={"all"})
     */
    private $tags;

    /**
     * @Vich\UploadableField(mapping="article_image", fileNameProperty="fileName")
     * @var File
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $fileName;

    /**
     * @var \DateTime
     * @ORM\Column(name="update_at", type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Author", inversedBy="articles")
     */
    private $author;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $authorComment;

    /**
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isPublished;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $keywords;

    /**
     * @ORM\Column(type="integer", options={"defaults": 0})
     */
    private $views;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="article", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\Column(type="integer", options={"defaults": 0})
     */
    private $commentCount;

    public function __construct()
    {
        $this->publishedAt = new \DateTime();
        $this->categories = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->views = 0;
        $this->comments = new ArrayCollection();
        $this->commentCount = 0;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        $slugify = new Slugify(['rulesets' => ['default', 'russian', 'ukrainian']]);
        $this->setSlug($slugify->slugify($title));

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getShortContent(): ?string
    {
        return $this->shortContent;
    }

    public function setShortContent(?string $shortContent): self
    {
        $this->shortContent = $shortContent;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addArticle($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeArticle($this);
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->addArticle($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removeArticle($this);
        }

        return $this;
    }

    /**
     * @return Tag[]
     */
    public function getRegularTags()
    {
        $regularTags = [];

        foreach ($this->getTags() as $tag) {
            if (!$tag->getIsSpecial()) {
                $regularTags[] = $tag;
            }
        }

        return $regularTags;
    }

    /**
     * @return File
     */
    public function getImage(): ? File
    {
        return $this->image;
    }

    /**
     * @param File $image
     * @return Article
     */
    public function setImage(?File $image): Article
    {

        if (null !== $image) {
            $this->image = $image;
            $this->updateAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     * @return Article
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateAt(): \DateTime
    {
        return $this->updateAt;
    }

    /**
     * @param \DateTime $updateAt
     * @return Article
     */
    public function setUpdateAt(\DateTime $updateAt): Article
    {
        $this->updateAt = $updateAt;
        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getAuthorComment(): ?string
    {
        return $this->authorComment;
    }

    public function setAuthorComment(?string $authorComment): self
    {
        $this->authorComment = $authorComment;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function getViews(): ?int
    {
        return $this->views;
    }

    public function setViews(int $views): self
    {
        $this->views = $views;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setArticle($this);
            $this->commentCount++;
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            $this->commentCount--;
            // set the owning side to null (unless already changed)
            if ($comment->getArticle() === $this) {
                $comment->setArticle(null);
            }
        }

        return $this;
    }

    public function getCommentCount(): ?int
    {
        return $this->commentCount;
    }

    public function setCommentCount(int $commentCount): self
    {
        $this->commentCount = $commentCount;

        return $this;
    }

    public function getRSSContent(): string
    {
        $content = $this->getContent();
        $content = preg_replace('#&\w+;#um', ' ', $content);
        $content = str_replace(["\n", "\r"], '', $content);
        $content = str_replace("</p>", "</p>\n\n", $content);
        $content = strip_tags($content);
        $content = preg_replace('@(\s*\n\s*)+@mu', "\n\n", $content);

        return $content;
    }

}
