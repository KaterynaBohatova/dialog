<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Author;
use App\Entity\Category;
use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @param $tagId
     * @param $limit
     * @return Article[]
     */
    public function findByTagId($tagId, $limit)
    {
        return $this->createQueryBuilder('a')
            ->join('a.tags', 't')
            ->where('t.id = :tagId')
            ->andWhere('a.isPublished = true')
            ->setParameter('tagId', $tagId)
            ->setMaxResults($limit)
            ->orderBy('a.publishedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getQBByAuthor(Author $author)
    {
        return $this->createQueryBuilder('a')
            ->join('a.author', 'aa')
            ->where('aa = :author')
            ->andWhere('a.isPublished = true')
            ->setParameter('author', $author);
    }

    public function getQBCategory(Category $category)
    {
        return $this->createQueryBuilder('a')
            ->join('a.categories', 'ac')
            ->where('ac = :category')
            ->andWhere('a.isPublished = true')
            ->orderBy('a.publishedAt', 'DESC')
            ->setParameter('category', $category);
    }

    public function getQBTag(Tag $tag)
    {
        return $this->createQueryBuilder('a')
            ->join('a.tags', 'at')
            ->where('at = :tag')
            ->andWhere('a.isPublished = true')
            ->setParameter('tag', $tag);
    }

    public function findMostCommented($limit)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.isPublished = true')
            ->orderBy('a.commentCount', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
