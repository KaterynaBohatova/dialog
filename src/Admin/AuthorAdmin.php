<?php

namespace App\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\Form\Type\DateTimePickerType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AuthorAdmin extends AbstractAdmin
{

    public function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name')
            ->add('image', VichImageType::class, [
                'label' => 'label.image_author',
                'required' => false,
            ])
            ->add('facebook')
            ->add('instagram')
            ->add('twitter')
            ->add('linkedin')
            ->add('publishedAt', DateTimePickerType::class)
            ->add('comment', null, [
                'label' => 'label.comment'
            ]);
    }

    public function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('name')
            ->add('image', null, [
                'label' => 'label.author_photo'
            ])
            ->add('publishedAt')
            ->add('comment', null, [
                'label' => 'label.author_comment'
            ]);
    }
}
