<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\Form\Type\DateTimePickerType;

class CommentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('article', null, [
                'label' => 'label.article'
            ])
            ->add('comment', null, [
                'label' => 'label.comment'
            ])
            ->add('createdAt', DateTimePickerType::class, [
                'label' => 'label.createdAt'
            ])
            ->add('name', null, [
                'label' => 'label.name_comment'
            ])
            ->add('email')
        ;

    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('article', null, [
                'label' => 'label.article'
            ])
            ->addIdentifier('comment', null, [
                'label' => 'label.comment'
            ])
            ->addIdentifier('createdAt', null, [
                'label' => 'label.createdAt'
            ])
            ->addIdentifier('name', null,  [
                'label' => 'label.name_comment'
            ])
            ->addIdentifier('email')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('article', null, [
                'label' => 'label.article'
            ])
            ->add('comment', null, [
                'label' => 'label.comment'
            ])
            ->add('name', null, [
                'label' => 'label.name_comment'
            ])
            ->add('email')
            ;
    }

}