<?php

namespace App\Admin;

use App\Entity\Category;
use App\Entity\Tag;
use Doctrine\ORM\Mapping\Entity;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\Form\Type\CollectionType;
use Sonata\Form\Type\DatePickerType;
use Sonata\Form\Type\DateTimePickerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ArticleAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'publishedAt'
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('isPublished', null, [
                'label' => 'Опублікувати'
            ])
            ->add('tags')
            ->add('title', null, [
                'label' => 'label.title'
            ])
            ->add('keywords', null, [
                'label' => 'SEO (вносить слова через запятую)'
            ])
            ->add('content', TextareaType::class, [
                'label' => 'label.content',
                'required' => false,
                'attr' => [
                    'class' => 'tinymce',
                ]
            ])
            ->add('image', VichImageType::class, [
                'label' => 'label.image',
                'required' => false,
            ])
            ->add('shortContent', TextareaType::class, [
                'label' => 'label.short_content'
            ])
            ->add('publishedAt', DateTimePickerType::class, [
                'label' => 'label.published_at'
            ])
            ->add('categories', EntityType::class, [
                'class' => Category::class,
                'multiple' => true,
                'label' => 'label.categories'
            ])
            ->add('source', null, [
                'label' => 'label.source'
            ])
            ->add('author', null, [
                'label' => 'label.author'
            ])
            ->add('authorComment', null, [
                'label' => 'label.author_comment'
            ])
        ;

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMaper)
    {
        $datagridMaper
            ->add('title')
            ->add('source')
            ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->addIdentifier('publishedAt')
            ->addIdentifier('source')
            ->add('categories')
            ->add('tags')
            ;
    }

}
