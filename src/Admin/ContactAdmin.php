<?php

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\AbstractAdmin;

class ContactAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('name', null, [
                'label' => 'label.name_contact'
            ])
            ->add('message', null, [
                'label' => 'label.message'
            ])
            ->add('email')
        ;

    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('name', null, [
                'label' => 'label.name_contact'
            ])
            ->addIdentifier('email')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name', null, [
                'label' => 'label.name_contact'
            ])
            ->add('message', null, [
                'label' => 'label.comment'
            ])
            ->add('email')
        ;
    }

}