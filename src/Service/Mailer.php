<?php

namespace App\Service;


use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment;

class Mailer
{

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var Environment
     */
    protected $twig;

    /**
     * @var string $mailerFrom
     */
    protected $mailerFrom;

    public function __construct(\Swift_Mailer $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->mailerFrom = getenv('MAILER_FROM');
    }

    /**
     * @param $subject
     * @param $mailTo
     * @param $template
     * @param $params
     */
    public function send($subject, $mailTo, $template, $params)
    {
        $content = $this->twig->render($template, $params);
        $message = new \Swift_Message();

        $message
            ->setSubject($subject)
            ->setFrom($this->mailerFrom)
            ->setTo($mailTo)
            ->setContentType('text/html')
            ->setBody($content);

        $this->mailer->send($message);
    }

}
